package br.com.razecApps.dribbblethumbs.core;

import br.com.razecApps.dribbblethumbs.services.DribbbleWebApi;
import javax.inject.Inject;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.Test;

/**
 * Created by marcoscezar on 13/07/15.
 */
public class FunctionalTests {

  @Inject
  DribbbleWebApi dribbbleWebApi;

  @Test
  public void testSomething() {
    System.out.println("Tests will begin...");
    MatcherAssert.assertThat(dribbbleWebApi, CoreMatchers.notNullValue());

  }

}
