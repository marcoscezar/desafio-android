package br.com.razecApps.dribbblethumbs.core;

import android.app.Instrumentation;
import android.content.IntentFilter;
import android.test.ActivityInstrumentationTestCase2;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;
import br.com.razecApps.dribbblethumbs.activities.DribbbleDetailActivity_;
import br.com.razecApps.dribbblethumbs.activities.MainActivity_;
import com.robotium.solo.Solo;
import com.squareup.spoon.Spoon;


/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class MainFlow extends ActivityInstrumentationTestCase2<MainActivity_> {

  private Solo solo;

  private Instrumentation instrumentation;
  private MainActivity_ activity;

  public MainFlow() {
    super(MainActivity_.class);
  }

  @Override
  protected void setUp() throws Exception {
    solo = new Solo(getInstrumentation(), getActivity());
    instrumentation = getInstrumentation();
    activity = getActivity();
  }


  public void testBasicFlow() {

    IntentFilter filter = new IntentFilter();
    Instrumentation.ActivityMonitor monitor = instrumentation.addMonitor(filter, null, false);

    RelativeLayout progressLayout = (RelativeLayout) solo.getView(R.id.progressLayoutId);
    solo.waitForCondition(() -> progressLayout.getVisibility() == View.GONE, 10000);


    ListView listView = (ListView) solo.getView(R.id.dribblePhotosListId);
    solo.waitForCondition(() -> listView.getVisibility() == View.VISIBLE, 10000);

    Spoon.screenshot(activity, "start_screen");

    solo.clickInList(1);
    solo.waitForActivity(DribbbleDetailActivity_.class, 10000);

    Spoon.screenshot(monitor.getLastActivity(), "next_activity_shown");

  }


}
