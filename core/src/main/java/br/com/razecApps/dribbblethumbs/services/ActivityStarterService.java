package br.com.razecApps.dribbblethumbs.services;

import android.content.Intent;
import br.com.razecApps.dribbblethumbs.events.StartActivityEvent;
import br.com.razecApps.dribbblethumbs.utils.ActivityExtraKeys;
import de.greenrobot.event.EventBus;
import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;

/**
 * Created by marcoscezar on 12/07/15.
 */
@EBean
public class ActivityStarterService {

  private EventBus eventBus = EventBus.getDefault();

  @AfterInject
  public void subscribe() {
    eventBus.register(this);
  }

  public void unsubscribe() {
    eventBus.unregister(this);
  }


  public void onEvent(StartActivityEvent startActivityEvent) {

    Intent intent = new Intent(startActivityEvent.getContext(), startActivityEvent
        .getTargetActivityCls());
    intent.putExtra(ActivityExtraKeys.IMAGE_ID, startActivityEvent.getDribbbleShotId());
    startActivityEvent.getContext().startActivity(intent);

  }


}

