package br.com.razecApps.dribbblethumbs.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by marcoscezar on 10/07/15.
 */
@Data
@JsonIgnoreProperties({"created_at", "image_teaser_url",
    "rebound_source_id", "short_url", "url", "rebounds_count", "comments_count",
    "likes_count", "description"})
public class DribbbleShot {


  @JsonProperty("id")
  private Long id;

  @JsonProperty("image_url")
  private String imageUrl;

  @JsonProperty("image_400_url")
  private String imageThumb;

  @JsonProperty("width")
  private Long width;

  @JsonProperty("height")
  private Long height;

  @JsonProperty("views_count")
  private Integer viewsCount;

  @JsonProperty("title")
  private String title;

  @JsonProperty("player")
  private DribbblePlayer player;

  public DribbbleShot() {

  }

}
