package br.com.razecApps.dribbblethumbs.views;

import android.content.Context;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import br.com.razecApps.dribbblethumbs.core.R;
import br.com.razecApps.dribbblethumbs.model.DribbbleShot;
import br.com.razecApps.dribbblethumbs.utils.ImageParamUtils;
import com.bumptech.glide.DrawableTypeRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

/**
 * Created by marcoscezar on 11/07/15.
 */
@EViewGroup(R.layout.dribbble_image_list_item)
public class DribbbleContentLayout extends RelativeLayout {

  @ViewById(R.id.dribbbleContentImageId)
  ImageView dribbbleContentImage;

  @ViewById(R.id.dribbbleContentTitleId)
  TextView dribbbleContentTitle;

  @ViewById(R.id.viewNumberId)
  TextView viewNumber;

  public DribbbleContentLayout(Context context) {
    super(context);
  }

  public void bind(DribbbleShot dribbbleShot) {

    DrawableTypeRequest<String> targetDrawable;

    if (!TextUtils.isEmpty(dribbbleShot.getImageThumb()) && dribbbleShot.getWidth() >=
        ImageParamUtils.MAX_WIDTH) {
      targetDrawable = Glide.with(getContext()).load(dribbbleShot
          .getImageThumb());
    } else {
      targetDrawable = Glide.with(getContext()).load(dribbbleShot.getImageUrl());
    }

    if (dribbbleShot.getImageUrl().contains(".gif")) {

      targetDrawable.asGif()
          .placeholder(R.mipmap.tiny_android_placeholder).fitCenter().centerCrop().crossFade()
          .diskCacheStrategy
              (DiskCacheStrategy.ALL).into
          (dribbbleContentImage);
    } else {
      targetDrawable
          .placeholder(R.mipmap.tiny_android_placeholder).fitCenter().centerCrop().crossFade()
          .diskCacheStrategy
              (DiskCacheStrategy.ALL).into
          (dribbbleContentImage);
    }

    dribbbleContentImage.setScaleType(ImageView.ScaleType.CENTER);

    dribbbleContentTitle.setText(dribbbleShot.getTitle());
    viewNumber.setText(String.valueOf(dribbbleShot.getViewsCount()));

  }

}
