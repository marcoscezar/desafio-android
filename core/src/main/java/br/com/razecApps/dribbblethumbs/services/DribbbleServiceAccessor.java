package br.com.razecApps.dribbblethumbs.services;

import android.util.Log;
import br.com.razecApps.dribbblethumbs.components.DaggerDribbbleServiceComponent;
import br.com.razecApps.dribbblethumbs.components.DribbbleServiceComponent;
import br.com.razecApps.dribbblethumbs.events.DribbbleGetImageDetailRequestEvent;
import br.com.razecApps.dribbblethumbs.events.DribbbleGetPopularRequestEvent;
import br.com.razecApps.dribbblethumbs.events.DribbbleImageDetailErrorResponseEvent;
import br.com.razecApps.dribbblethumbs.events.DribbbleImageDetailSuccessEvent;
import br.com.razecApps.dribbblethumbs.events.DribbblePopularErrorResponseEvent;
import br.com.razecApps.dribbblethumbs.events.DribbblePopularMoreSuccessResponseEvent;
import br.com.razecApps.dribbblethumbs.events.DribbblePopularSuccessResponseEvent;
import br.com.razecApps.dribbblethumbs.model.DribbbleContent;
import br.com.razecApps.dribbblethumbs.model.DribbbleDetail;
import br.com.razecApps.dribbblethumbs.modules.DribbbleServiceModule;
import de.greenrobot.event.EventBus;
import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by marcoscezar on 09/07/15.
 */
@EBean
public class DribbbleServiceAccessor {

  private DribbbleWebApi dribbbleWebApi;

  private EventBus eventBus = EventBus.getDefault();

  public static final String TAG = DribbbleServiceAccessor.class.getSimpleName();

  @AfterInject
  public void setup() {

    DribbbleServiceComponent dribbbleServiceComponent = DaggerDribbbleServiceComponent.builder()
        .dribbbleServiceModule(new DribbbleServiceModule()).build();

    dribbbleWebApi = dribbbleServiceComponent.provideDribbbleWebApi();

    eventBus.register(this);
  }


  public void cleanup() {
    eventBus.unregister(this);
  }


  public void onEvent(DribbbleGetPopularRequestEvent dribbbleGetPopularRequestEvent) {

    dribbbleWebApi.getPopularImages(dribbbleGetPopularRequestEvent.getPage(), new
        Callback<DribbbleContent>() {

          @Override
          public void success(DribbbleContent dribbbleContent, Response response) {
            Log.d(TAG, "Inside getPopular()");
            Log.d(TAG, "Numero de registros: " + dribbbleContent
                .getShots().size());

            if (!dribbbleGetPopularRequestEvent.isMore()) {
              eventBus.post(new DribbblePopularSuccessResponseEvent(dribbbleContent));
            } else {
              eventBus.post(new DribbblePopularMoreSuccessResponseEvent(dribbbleContent));
            }

          }

          @Override
          public void failure(RetrofitError error) {

            eventBus.post(new DribbblePopularErrorResponseEvent(error));

          }
        });

  }

  public void onEvent(DribbbleGetImageDetailRequestEvent dribbbleGetImageDetailRequestEvent) {

    dribbbleWebApi.getImageDetail(dribbbleGetImageDetailRequestEvent.getDribbbleShotId(), new
        Callback<DribbbleDetail>() {

          @Override
          public void success(DribbbleDetail dribbbleDetail, Response response) {
            Log.d(TAG, "Inside getImageDetails()");

            eventBus.post(new DribbbleImageDetailSuccessEvent(dribbbleDetail));

          }

          @Override
          public void failure(RetrofitError error) {
            eventBus.post(new DribbbleImageDetailErrorResponseEvent(error));
          }
        });

  }



}
