package br.com.razecApps.dribbblethumbs.activities;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import br.com.razecApps.dribbblethumbs.adapters.DribbbleContentsAdapter;
import br.com.razecApps.dribbblethumbs.core.R;
import br.com.razecApps.dribbblethumbs.events.DribbbleGetPopularRequestEvent;
import br.com.razecApps.dribbblethumbs.events.DribbblePopularErrorResponseEvent;
import br.com.razecApps.dribbblethumbs.events.DribbblePopularMoreSuccessResponseEvent;
import br.com.razecApps.dribbblethumbs.events.DribbblePopularSuccessResponseEvent;
import br.com.razecApps.dribbblethumbs.events.StartActivityEvent;
import br.com.razecApps.dribbblethumbs.model.DribbbleShot;
import br.com.razecApps.dribbblethumbs.services.ActivityStarterService;
import br.com.razecApps.dribbblethumbs.services.DribbbleServiceAccessor;
import br.com.razecApps.dribbblethumbs.views.custom.InfiniteScrollAdapter;
import com.bumptech.glide.Glide;
import com.bumptech.glide.MemoryCategory;
import de.greenrobot.event.EventBus;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;


@EActivity(R.layout.activity_main)
@OptionsMenu(R.menu.menu_main)
public class MainActivity extends AppCompatActivity implements InfiniteScrollAdapter
    .InfiniteScrollListener {

  private static final String TAG = MainActivity.class.getSimpleName();

  @ViewById(R.id.progressLayoutId)
  RelativeLayout progressLayout;

  @ViewById(R.id.dribblePhotosListId)
  ListView dribbbleListView;

  @ViewById(R.id.dribbbleSwipeViewId)
  SwipeRefreshLayout swipeRefreshLayout;

  @ViewById(R.id.requestErrorLayout)
  RelativeLayout errorLayout;

  @Bean
  DribbbleServiceAccessor dribbbleServiceAccessor;

  private InfiniteScrollAdapter<DribbbleContentsAdapter> mAdapter;

  @Bean
  DribbbleContentsAdapter dribbbleContentsAdapter;

  @Bean
  ActivityStarterService activityStarterService;

  private EventBus eventBus = EventBus.getDefault();

  private int currentPage = 1;


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.activity_main);
    eventBus.register(this);

    Glide.get(this).setMemoryCategory(MemoryCategory.HIGH);
  }

  @AfterViews
  public void initializeView() {

    showProgress();
    setupSwipeToRefresh();

    eventBus.post(new DribbbleGetPopularRequestEvent(currentPage, false));

  }

  private void setupSwipeToRefresh() {
    swipeRefreshLayout.setOnRefreshListener(() -> {

      swipeRefreshLayout.setRefreshing(true);
      //NOTE Solicita a chamada para que a classe de webservice realize a chamada ao
      // flickr e obtenha os registros.
      currentPage = 1;

      eventBus.post(new DribbbleGetPopularRequestEvent(currentPage, false));
      showProgress();
    });

  }


  private void showProgress() {

    progressLayout.setVisibility(View.VISIBLE);
    swipeRefreshLayout.setVisibility(View.GONE);
    dribbbleListView.setVisibility(View.GONE);
  }

  private void hideProgress() {
    progressLayout.setVisibility(View.GONE);
    swipeRefreshLayout.setVisibility(View.VISIBLE);
    dribbbleListView.setVisibility(View.VISIBLE);
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();

    eventBus.unregister(this);
    dribbbleServiceAccessor.cleanup();

  }

  @ItemClick(R.id.dribblePhotosListId)
  public void selectShotItem(DribbbleShot dribbbleShot) {
    Log.d(TAG, "MainActivity.selectShotItem");
    eventBus.post(new StartActivityEvent(this, String.valueOf(dribbbleShot.getId()),
        DribbbleDetailActivity_.class));
  }


  public void onEvent(DribbblePopularSuccessResponseEvent responseEvent) {

    updateAdapter(responseEvent);

  }

  @UiThread
  void updateAdapter(DribbblePopularSuccessResponseEvent responseEvent) {
    RelativeLayout progress = new RelativeLayout(this);
    progress.setLayoutParams(new GridView.LayoutParams(GridView.LayoutParams.MATCH_PARENT,
        100));
    progress.setGravity(Gravity.CENTER);
    progress.addView(new ProgressBar(this));

    dribbbleContentsAdapter.clear();
    dribbbleContentsAdapter.addAllItems(responseEvent.getDribbbleContent().getShots());

    mAdapter = new InfiniteScrollAdapter<>(this, dribbbleContentsAdapter, progress);
    mAdapter.addListener(this);

    dribbbleListView.setAdapter(mAdapter);
    hideErrorPage();
    hideProgress();
    swipeRefreshLayout.setRefreshing(false);
  }


  public void onEvent(DribbblePopularMoreSuccessResponseEvent moreSuccessResponseEvent) {

    addMoreUpdate(moreSuccessResponseEvent);

  }

  @UiThread
  void addMoreUpdate(DribbblePopularMoreSuccessResponseEvent moreSuccessResponseEvent) {
    dribbbleContentsAdapter.addAllItems(moreSuccessResponseEvent.getDribbbleContent().getShots());

    ((BaseAdapter) dribbbleListView.getAdapter()).notifyDataSetChanged();
    mAdapter.handledRefresh();
  }


  public void onEvent(DribbblePopularErrorResponseEvent responseErrorEvent) {

    hideProgress();

    if (responseErrorEvent.getRetrofitError().getResponse().getStatus() >= 400 &&
        responseErrorEvent.getRetrofitError().getResponse().getStatus() < 500) {
      swipeRefreshLayout.setRefreshing(false);
      showErrorPage();
    }

  }

  private void showErrorPage() {
    errorLayout.setVisibility(View.VISIBLE);
  }

  private void hideErrorPage() {
    errorLayout.setVisibility(View.GONE);
  }

  @Override
  public void onInfiniteScrolled() {
    currentPage++;
    eventBus.post(new DribbbleGetPopularRequestEvent(currentPage, true));
  }
}
