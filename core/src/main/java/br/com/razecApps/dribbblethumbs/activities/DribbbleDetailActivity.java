package br.com.razecApps.dribbblethumbs.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import br.com.razecApps.dribbblethumbs.core.R;
import br.com.razecApps.dribbblethumbs.events.DribbbleGetImageDetailRequestEvent;
import br.com.razecApps.dribbblethumbs.events.DribbbleImageDetailErrorResponseEvent;
import br.com.razecApps.dribbblethumbs.events.DribbbleImageDetailSuccessEvent;
import br.com.razecApps.dribbblethumbs.services.DribbbleServiceAccessor;
import br.com.razecApps.dribbblethumbs.utils.ActivityExtraKeys;
import br.com.razecApps.dribbblethumbs.utils.ImageParamUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import de.greenrobot.event.EventBus;
import jp.wasabeef.glide.transformations.CropCircleTransformation;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.ViewById;
import retrofit.RetrofitError;

/**
 * Created by marcoscezar on 12/07/15.
 */
@EActivity(R.layout.activity_dribbble_detail)
@OptionsMenu(R.menu.menu_dribbble_detail)
public class DribbbleDetailActivity extends AppCompatActivity {


  private static final String TAG = DribbbleDetailActivity.class.getSimpleName();

  @ViewById(R.id.playerPhotoId)
  ImageView playerPhoto;

  @ViewById(R.id.playerUsernameId)
  TextView playerUsername;

  @ViewById(R.id.dribbbleShotDetailId)
  TextView dribbbleShotDetail;

  private EventBus eventBus = EventBus.getDefault();

  @Bean
  DribbbleServiceAccessor dribbbleServiceAccessor;

  @ViewById(R.id.progressLayoutId)
  RelativeLayout progressLayout;

  @ViewById(R.id.detailContentLayoutId)
  RelativeLayout detailContentLayout;

  private String dribbbleShotId;

  @ViewById(R.id.detailImageContentId)
  ImageView detailImageContent;

  @ViewById(R.id.detailContentTitleId)
  TextView detailContentTitle;

  @ViewById(R.id.detailViewQuantityId)
  TextView detailViewCount;

  @ViewById(R.id.requestErrorLayout)
  RelativeLayout errorLayout;



  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    this.eventBus.register(this);

    if (getIntent() != null) {
      dribbbleShotId = getIntent().getStringExtra(ActivityExtraKeys.IMAGE_ID);

    }

  }


  @AfterViews
  public void setupView() {
    showProgress();
    eventBus.post(new DribbbleGetImageDetailRequestEvent(dribbbleShotId));

  }

  private void showProgress() {
    this.detailContentLayout.setVisibility(View.GONE);
    this.progressLayout.setVisibility(View.VISIBLE);
  }

  private void hideProgress() {
    this.progressLayout.setVisibility(View.GONE);
    this.detailContentLayout.setVisibility(View.VISIBLE);
  }


  @Override
  protected void onDestroy() {
    super.onDestroy();

    this.eventBus.unregister(this);
    dribbbleServiceAccessor.cleanup();
  }

  public void onEvent(DribbbleImageDetailSuccessEvent dribbbleImageDetailSuccessEvent) {

    hideErrorPage();
    hideProgress();

    fillMainImageInfs(dribbbleImageDetailSuccessEvent);
    fillPlayerData(dribbbleImageDetailSuccessEvent);


  }

  private void hideErrorPage() {
    errorLayout.setVisibility(View.GONE);
  }

  private void fillPlayerData(DribbbleImageDetailSuccessEvent dribbbleImageDetailSuccessEvent) {
    BitmapPool bitmapPool = Glide.get(this).getBitmapPool();

    Glide.with(this).load(dribbbleImageDetailSuccessEvent.getDribbbleDetail().getDribbblePlayer()
        .getAvatarUrl()).placeholder(R.mipmap.ic_profile_placeholder).bitmapTransform(new
        CropCircleTransformation(bitmapPool)).into
        (playerPhoto);


    playerUsername.setText(dribbbleImageDetailSuccessEvent.getDribbbleDetail().getDribbblePlayer
        ().getUsername());
  }

  private void fillMainImageInfs(DribbbleImageDetailSuccessEvent dribbbleImageDetailSuccessEvent) {

    String contentImageUrl;
    if (!TextUtils.isEmpty(dribbbleImageDetailSuccessEvent.getDribbbleDetail().getImageThumb())
        && dribbbleImageDetailSuccessEvent.getDribbbleDetail().getWidth() >= ImageParamUtils
        .MAX_WIDTH_DETAIL) {
      contentImageUrl = dribbbleImageDetailSuccessEvent.getDribbbleDetail().getImageThumb();
    } else {
      contentImageUrl = dribbbleImageDetailSuccessEvent.getDribbbleDetail().getImageUrl();
    }

    if (contentImageUrl.contains(".gif")) {
      Glide.with(this).load(contentImageUrl).asGif().crossFade().centerCrop().diskCacheStrategy
          (DiskCacheStrategy.ALL).placeholder(R.mipmap.tiny_android_placeholder).into
          (detailImageContent);

    } else {

      Glide.with(this).load(contentImageUrl).crossFade().centerCrop().diskCacheStrategy
          (DiskCacheStrategy.ALL).placeholder(R.mipmap.tiny_android_placeholder).into
          (detailImageContent);

    }

    detailImageContent.setScaleType(ImageView.ScaleType.CENTER);
    detailContentTitle.setText(dribbbleImageDetailSuccessEvent.getDribbbleDetail().getTitle());
    detailViewCount.setText(String.valueOf(dribbbleImageDetailSuccessEvent.getDribbbleDetail()
        .getViewCount()));

    String imageDetailDescription = dribbbleImageDetailSuccessEvent.getDribbbleDetail()
        .getDescription
            ();

    if (!TextUtils.isEmpty(imageDetailDescription)) {
      dribbbleShotDetail.setText(Html.fromHtml(imageDetailDescription));
    }

  }

  public void onEvent(DribbbleImageDetailErrorResponseEvent dribbbleImageDetailErrorResponseEvent) {
    Log.d(TAG, "DribbbleDetailActivity.onEvent");

    hideProgress();

    RetrofitError retrofitError = dribbbleImageDetailErrorResponseEvent.getRetrofitError();

    if (retrofitError.getResponse().getStatus() >= 400 &&
        retrofitError.getResponse().getStatus() < 500) {
      showErrorPage();
    }

  }

  private void showErrorPage() {
    errorLayout.setVisibility(View.VISIBLE);
  }

}
