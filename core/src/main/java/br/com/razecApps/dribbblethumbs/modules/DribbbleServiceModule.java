package br.com.razecApps.dribbblethumbs.modules;

import br.com.razecApps.dribbblethumbs.services.DribbbleWebApi;
import br.com.razecApps.dribbblethumbs.utils.WebServiceUtils;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;
import retrofit.RestAdapter;
import retrofit.converter.JacksonConverter;

/**
 * Created by marcoscezar on 09/07/15.
 */
@Module
public class DribbbleServiceModule {

  @Provides
  @Singleton
  DribbbleWebApi provideDribbbleWebApi() {
    return new RestAdapter.Builder().setConverter(new JacksonConverter()).setLogLevel(RestAdapter
        .LogLevel.FULL).setEndpoint
        (WebServiceUtils.BASE_URI).build().create(DribbbleWebApi.class);
  }

}
