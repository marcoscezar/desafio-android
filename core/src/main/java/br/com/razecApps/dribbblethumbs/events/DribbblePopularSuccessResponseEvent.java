package br.com.razecApps.dribbblethumbs.events;

import br.com.razecApps.dribbblethumbs.model.DribbbleContent;
import lombok.Value;

/**
 * Created by marcoscezar on 10/07/15.
 */
@Value
public class DribbblePopularSuccessResponseEvent {
  private DribbbleContent dribbbleContent;
}
