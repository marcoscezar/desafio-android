package br.com.razecApps.dribbblethumbs.events;

import lombok.Value;

/**
 * Created by marcoscezar on 10/07/15.
 */
@Value
public class DribbbleGetPopularRequestEvent {

  private int page;

  private boolean more;

}
