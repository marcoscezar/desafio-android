package br.com.razecApps.dribbblethumbs.services;

import br.com.razecApps.dribbblethumbs.model.DribbbleContent;
import br.com.razecApps.dribbblethumbs.model.DribbbleDetail;
import br.com.razecApps.dribbblethumbs.model.DribbbleShot;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by marcoscezar on 09/07/15.
 */
public interface DribbbleWebApi {

  @GET("/shots/popular")
  void getPopularImages(@Query("page") int page, Callback<DribbbleContent> callbackHandler);

  @GET("/shots/{id}")
  void getImageDetail(@Path("id") String shotId, Callback<DribbbleDetail> callbackHandler);
}
