package br.com.razecApps.dribbblethumbs.events;

import br.com.razecApps.dribbblethumbs.model.DribbbleDetail;
import lombok.Value;

/**
 * Created by marcoscezar on 12/07/15.
 */
@Value
public class DribbbleImageDetailSuccessEvent {
  private DribbbleDetail dribbbleDetail;
}
