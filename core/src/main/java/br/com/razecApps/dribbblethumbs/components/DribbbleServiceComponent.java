package br.com.razecApps.dribbblethumbs.components;

import br.com.razecApps.dribbblethumbs.modules.DribbbleServiceModule;
import br.com.razecApps.dribbblethumbs.services.DribbbleWebApi;
import dagger.Component;
import javax.inject.Singleton;

/**
 * Created by marcoscezar on 09/07/15.
 */
@Singleton
@Component(modules = {DribbbleServiceModule.class})
public interface DribbbleServiceComponent {
  public DribbbleWebApi provideDribbbleWebApi();
}
