package br.com.razecApps.dribbblethumbs.events;

import android.app.Activity;
import android.content.Context;
import lombok.Value;

/**
 * Created by marcoscezar on 12/07/15.
 */
@Value
public class StartActivityEvent {

  private Context context;
  private String dribbbleShotId;
  private Class<? extends Activity> targetActivityCls;


}
