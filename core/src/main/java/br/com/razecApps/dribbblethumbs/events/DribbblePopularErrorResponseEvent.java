package br.com.razecApps.dribbblethumbs.events;

import lombok.Value;
import retrofit.RetrofitError;

/**
 * Created by marcoscezar on 10/07/15.
 */
@Value
public class DribbblePopularErrorResponseEvent {
  private RetrofitError retrofitError;
}
