package br.com.razecApps.dribbblethumbs.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by marcoscezar on 12/07/15.
 */


@Data
@JsonIgnoreProperties({
    "created_at", "image_teaser_url", "rebound_source_id",
    "short_url", "url", "rebounds_count", "comments_count", "likes_count",
    "id"
})
public class DribbbleDetail {

  @JsonProperty("player")
  private DribbblePlayer dribbblePlayer;

  @JsonProperty("image_url")
  private String imageUrl;

  @JsonProperty("image_400_url")
  private String imageThumb;

  @JsonProperty("title")
  private String title;

  @JsonProperty("views_count")
  private Integer viewCount;

  @JsonProperty("description")
  private String description;

  @JsonProperty("width")
  private int width;

  @JsonProperty("height")
  private int height;

}
