package br.com.razecApps.dribbblethumbs.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import lombok.Data;

/**
 * Created by marcoscezar on 09/07/15.
 */
@Data
public class DribbbleContent {

  @JsonProperty("shots")
  private List<DribbbleShot> shots;

  @JsonProperty("total")
  private int total;

  @JsonProperty("pages")
  private int pages;

  @JsonProperty("per_page")
  private int perPage;

  @JsonProperty("page")
  private int page;

  public DribbbleContent() {

  }

}
