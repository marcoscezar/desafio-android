package br.com.razecApps.dribbblethumbs.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import br.com.razecApps.dribbblethumbs.model.DribbbleShot;
import br.com.razecApps.dribbblethumbs.views.DribbbleContentLayout;
import br.com.razecApps.dribbblethumbs.views.DribbbleContentLayout_;
import java.util.ArrayList;
import java.util.List;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

/**
 * Created by marcoscezar on 11/07/15.
 */
@EBean
public class DribbbleContentsAdapter extends BaseAdapter {

  @RootContext
  Context context;

  List<DribbbleShot> items = new ArrayList<>();

  @Override
  public int getCount() {
    return items.size();
  }

  @Override
  public DribbbleShot getItem(int position) {
    return items.get(position);
  }

  @Override
  public long getItemId(int position) {
    return position;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {


    DribbbleContentLayout dribbbleContentLayout;
    if (convertView == null) {
      dribbbleContentLayout = DribbbleContentLayout_.build(context);
    } else {
      dribbbleContentLayout = (DribbbleContentLayout) convertView;
    }

    if (items.size() > 0) {
      dribbbleContentLayout.bind(getItem(position));
    }

    return dribbbleContentLayout;
  }


  public void addAllItems(List<DribbbleShot> shots) {
    this.items.addAll(shots);
  }

  public void clear() {
    this.items.clear();
  }


}
