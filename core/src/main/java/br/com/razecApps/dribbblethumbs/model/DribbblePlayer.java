package br.com.razecApps.dribbblethumbs.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * Created by marcoscezar on 10/07/15.
 */
@Data
@JsonIgnoreProperties({"created_at", "following_count", "shots_count", "drafted_by_player_id",
    "website_url", "twitter_screen_name", "url",
    "rebounds_received_count", "rebounds_count", "comments_received_count", "comments_count",
    "likes_received_count", "likes_count", "draftees_count", "followers_count", "location",
    "id"})
public class DribbblePlayer {

  @JsonProperty("name")
  private String name;

  @JsonProperty("username")
  private String username;

  @JsonProperty("avatar_url")
  private String avatarUrl;

  public DribbblePlayer() {

  }

}
