package br.com.razecApps.dribbblethumbs.events;

import lombok.Value;
import retrofit.RetrofitError;

/**
 * Created by marcoscezar on 12/07/15.
 */
@Value
public class DribbbleImageDetailErrorResponseEvent {
  private RetrofitError retrofitError;
}
