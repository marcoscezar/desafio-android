package br.com.razecApps.dribbblethumbs.events;

import lombok.Value;

/**
 * Created by marcoscezar on 12/07/15.
 */
@Value
public class DribbbleGetImageDetailRequestEvent {

  private String dribbbleShotId;

}
